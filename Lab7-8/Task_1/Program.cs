﻿using System;
using System.Collections.Generic;
using System.Text;

    class Program
    {
        static string[] City = { "Київ", "Житомир", "Новоград-Волинський", "Рівне", "Луцьк", "Бердичів", "Вінниця", "Хмельницький", "Тернопіль", "Шепетівка", "Умань", "Черкаси", "Кременчуг", "Полтава", "Харків", "Прилуки", "Суми", "Миргород", "Біла Церква" };
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;
            int[,] graph =
            {
                {0,135,0,0,0,0,0,0,0,0,0,0,0,0,0,128,0,0,78 },
                {0,0,80,0,0,38,0,0,0,115,0,0,0,0,0,0,0,0,0 },
                {0,0,0,100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,68,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,73,0,0,0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,110,0,0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,104,0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0,0,0,105,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,130,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,175,109,0 },
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0,115,146,0,181,0,0,0,0,0 },
            };
            int dist = 0;
            string begin = "";
            int j = 0;
            bool[] okDFS = new bool[graph.GetLength(0)];
            bool[] okBFS = new bool[graph.GetLength(0)];
           
            Console.WriteLine("BFS");
            BFS(j, graph, okBFS);
        Console.WriteLine("\n");
        Console.WriteLine("DFS");
        DFS(j, graph, okDFS, dist, begin);
    
    }
        static void DFS(int j, int[,] graph, bool[] visited, int dist = 0, string begin = "")
        {
            if (begin == "")
                begin = City[j];
            visited[j] = true;
            for (int i = 0; i < graph.GetLength(0); i++)
            {
                if (graph[j, i] != 0 && visited[i] != true)
                {
                    int finaldist = dist + graph[j, i];
                    Console.WriteLine(begin + " -> " + City[i] + ": " + finaldist + "км");
                    DFS(i, graph, visited, finaldist, begin);
                }
            }
        }
        static void BFS(int j, int[,] graph, bool[] visited)
        {
            int[] dist = new int[graph.GetLength(0)];
            string begin = City[j];
            Queue<int> q = new Queue<int>();
            visited[j] = true;
            q.Enqueue(j);
            while (q.Count != 0)
            {
                j = q.Dequeue();
                for (int i = 0; i < graph.GetLength(0); i++)
                {
                    if (graph[j, i] != 0 && visited[i] != true)
                    {
                        visited[i] = true;
                        q.Enqueue(i);
                        dist[i] = dist[j] + graph[j, i];
                        Console.WriteLine(begin+ " -> " + City[i] + ": " + dist[i] + "км");
                    }
                }
            }
        }
    }






