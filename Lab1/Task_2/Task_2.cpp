﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <time.h>
#include <windows.h>

union Short
{
	signed short x;
	struct Bytes {
		unsigned short znach : 15;
		unsigned short znak : 1;
	}bites;
}str;
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	printf("Введіть число x="); scanf_s("%d", &str);
	printf("\nСтруктура даних: ");
	if (str.bites.znak == 1 && str.bites.znach != 0)
		printf("число від'ємне\n");
	else if (str.bites.znak == 0 && str.bites.znach != 0)
		printf("число додатнє\n");
	else
		printf("Число нейтральне\n");
	printf("\nЛогічна операція: ");
	if (str.x < 0)
		printf("Число від'ємне\n");
	else if (str.x > 0)
		printf("Число додатнє\n");
	else printf("Число нейтральне\n");
	printf("Значення числа: %d\n", str.x);
}