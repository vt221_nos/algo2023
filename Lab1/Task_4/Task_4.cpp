﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <time.h>
#include <windows.h>


union Func
{
	float Value;
	int ValueInt;
	struct
	{
		int mantissa : 23;
		int exp : 8;
		int znak : 1;
	} bits;

	void Bits()
	{
		int x = ValueInt;
		unsigned char b[33];
		for (char i = 1; i < 33; i++)
		{
			b[i] = x & 1;
			x >>= 1;
		}
		for (char i = 32; i >= 1; i--)
		{
			printf("%x", b[i]);
		}
	}

	void Bytes()
	{
		int x = ValueInt;
		short b[5];
		for (char i = 1; i < 5; i++)
		{
			b[i] = x & 255;
			x >>= 8;
		}
		for (char i = 4; i > 1; i--)
		{
			printf("%02x.", b[i]);
		}
		printf("%02x", b[1]);
	}
};

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	Func x;
	printf("Введіть дробове число: ");
	scanf_s("%f", &x.Value);
	printf("\n");
	printf("Побітово: ");
	x.Bits();
	printf("\n");
	printf("Побайтово: ");
	x.Bytes();
	printf("\n");
	printf("Експонента: %d\n", x.bits.exp);
	printf("Мантиса: %d\n", x.bits.mantissa);	
	printf("Знак: %c\n",x.bits.znak ? '+' : '-');
	return 0;
}