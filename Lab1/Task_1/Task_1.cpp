﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <time.h>
#include <windows.h>

struct Date
{
	unsigned short hrs : 5;
	unsigned short min : 7;
	unsigned short sec : 7;
	unsigned short day : 5;
	unsigned short weekday : 3;
	unsigned short month : 4;
	unsigned short year : 7;
};
enum weekday {
	Monday,
	Tuesday,
	Wednesday,
	Thursday,
	Friday,
	Saturday,
	Sunday
};

void dayweek(Date now) {
	switch (now.weekday)
	{
	case Monday: printf("Понеділок ");
		break;
	case Tuesday: printf("Вівторок ");
		break;
	case Wednesday: printf("Середа ");
		break;
	case Thursday: printf("Четвер ");
		break;
	case Friday: printf("П'ятниця ");
		break;
	case Saturday: printf("Субота ");
		break;
	case Sunday: printf("Неділя ");
		break;
	}
	printf("%d:%d:%d %d.%d.%d\n", now.hrs, now.min, now.sec, now.day, now.month, 2000+now.year);
}
int main()
{
	Date now = { 17, 31, 37, 17,Friday, 3, 23 };
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	dayweek(now);
	printf("Кількість байт, що займає структура: %d\n", sizeof(Date));
	printf("Кількість байт, що займає структура time.h: %d\n", sizeof(tm));
	system("pause");
}
