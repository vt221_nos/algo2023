﻿#include <iostream>
#include <Windows.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#define MAX 11000
int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    system("color F0");
    srand(time(0));
    int lengh = 10000;
    int temp;
    int array[MAX];
    for (int i = 0; i <= lengh; i++)
    {
        array[i] = rand() % 1001;
        printf("%d", array[i]);
    }
    float result;
    clock_t start, finish;
    start = clock();
    for (int i = 1; i < lengh + 1; i++)
    {
        temp = array[i];
        for (int j = i - 1; j >= 0 && array[j] > temp; j--)
        {
            array[j + 1] = array[j];
            array[j] = temp;
        }
    }
    finish = clock();
    printf("\n--------------\n");
    for (int i = 0; i < lengh + 1; i++)
    {
        printf("%d ", array[i]);
    }
    result = (float)(finish - start) / CLOCKS_PER_SEC;
    printf("\nЧас виконання програми: %.4f секунд", result);
}
