﻿#include <iostream>
#include <Windows.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#define MAX 11000
int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    system("color F0");
    srand(time(0));
    int lengh = 10000;
    int temp, minimum;
    int  array[MAX];
    for (int i = 0; i <= lengh; i++)
    {
        array[i] = rand() % 1001;
        printf("%d ", array[i]);
    }
    float result;
    clock_t start, finish;
    start = clock();
    for (int i = 0; i < lengh - 1; i++)
    {
        minimum = i;
        for (int j = i + 1; j < lengh + 1; j++)
            if (array[j] < array[minimum]) minimum = j;
        temp = array[i];
        array[i] = array[minimum];
        array[minimum] = temp;
    }
    finish = clock();
    printf("\n--------------\n");
    for (int i = 0; i < lengh + 1; i++)
    {
        printf("%d ", array[i]);
    }
    result = (float)(finish - start) / CLOCKS_PER_SEC;
    printf("\nЧас виконання програми: %.4f секунд", result);
}
