﻿#include <stdio.h>
#include <math.h>
#include <windows.h>

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    double a, result;
    FILE* f;

    if (fopen_s(&f, "Table.csv", "w") != 0) {
        printf("Помилка: Неможливо створити файл.\n");
        return 1;
    }

    fprintf(f, "n,f(n)=n,f(n)=log(n),f(n)=n*log(n),f(n)=n^2,f(n)=2^n,f(n)=n!\n");

    for (a = 0; a <= 50; a++) {
        result = a;
        fprintf(f, "%.0lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.0lf\n", a, result, log(a), a * log(a), pow(a, 2), pow(2, a), tgamma(a + 1));
    }

    fclose(f);

    printf("Результати збережено до файлу results.csv\n");
    return 0;
}