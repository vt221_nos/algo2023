﻿#include <iostream>
#include <windows.h> 
using namespace std;

int fib(int n) {
    if (n == 0) return 0;
    if (n == 1) return 1;
    return fib(n - 1) + fib(n - 2);
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int n;
    cout << "Введіть число n: ";
    cin >> n;
    if (n < 0 || n > 40) {
        cout << "Помилка: n має бути в діапазоні [0, 40]" << endl;
        return 0;
    }
    cout << "Число Фібоначчі для n=" << n << " дорівнює " << fib(n) << endl;
    return 0;
}