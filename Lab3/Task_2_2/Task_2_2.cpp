﻿#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include <windows.h> 
using namespace std;

int maxBinaryNum(vector<int>& digits) {
    sort(digits.begin(), digits.end(), greater<int>()); // сортуємо цифри у зворотному порядку
    int maxNum = 0;
    int powOfTwo = 1;
    for (int i = 0; i < digits.size(); i++) {
        maxNum += digits[i] * powOfTwo; // додаємо поточну цифру до максимального числа
        powOfTwo *= 2; // збільшуємо потужність двійки
    }
    return maxNum;
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int m;
    cout << "Введіть обсяг масиву m: ";
    cin >> m;
    if (m > 40) {
        cout << "Помилка: m має бути не більшим за 40" << endl;
        return 0;
    }
    vector<int> digits(m);
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> distrib(0, 1);

    for (int i = 0; i < m; i++) {
        digits[i] = distrib(gen); // генеруємо випадкову цифру (0 або 1)
    }

    cout << "Згенерований масив цифр: ";
    for (int i = 0; i < m; i++) {
        cout << digits[i];
    }
    cout << endl;

    int maxNum = maxBinaryNum(digits);
    cout << "Найбільше можливе число з даних цифр: " << maxNum << endl;

    return 0;
}
