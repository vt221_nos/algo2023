﻿#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>

struct DoubleList
{
    int data;
    struct DoubleList* next;
    struct DoubleList* prev;
};

struct DoubleList* head = NULL;
//ДОДАВАННЯ ЕЛЕМЕНТА
void AddList(int value, int position)
{
    struct DoubleList* node = (struct DoubleList*)malloc(sizeof(struct DoubleList));
    node->data = value;
    if (head == NULL)
    {
        node->next = node;
        node->prev = node;
        head = node;
    }
    else
    {
        struct DoubleList* p = head;
        for (int i = position; i > 1; i--)
        {
            p = p->next;
        }
        p->prev->next = node;
        node->prev = p->prev;
        node->next = p;
        p->prev = node;
    }
    printf("\nЭлемент добавлен...\n\n");
}
//ВИДАЛЕННЯ ЭЛЕМЕНТА
void DeleteList(int position)
{
    if (head == NULL)
    {
        printf("\nСписок пуст\n\n");
        return;
    }
    if (head == head->next)
    {
        free(head);
        head = NULL;
    }
    else
    {
        struct DoubleList* a = head;
        for (int i = position; i > 1; i--)
        {
            a = a->next;
        }
        if (a == head)
        {
            head = a->next;
        }
        a->prev->next = a->next;
        a->next->prev = a->prev;
        free(a);
    }
    printf("\nЭлемент удален...\n\n");
}
//ВИВЕДЕННЯ СПИСКУ ЕЛЕМЕНТІВ
void PrintList()
{
    if (head == NULL)
        printf("\nСписок пуст\n\n");
    else
    {
        struct DoubleList* a = head;
        printf("\nЭлементы списка: ");
        do
        {
            printf("%d ", a->data);
            a = a->next;
        } while (a != head);
        printf("\n\n");
    }
}

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int value, position, x;
    do
    {
        printf("1. Добавить элемент\n");
        printf("2. Удалить элемент\n");
        printf("3. Вывести список\n");
        printf("0. Выйти\n");
        printf("\nНомер операции > ");
        scanf_s("%d", &x);
        switch (x)
        {
        case 1:
            printf("Значение > ");
            scanf_s("%d", &value);
            printf("Позиция > ");
            scanf_s("%d", &position);
            AddList(value, position);
            break;
        case 2:
            printf("Позиция > ");
            scanf_s("%d", &position);
            DeleteList(position);
            break;
        case 3:
            PrintList();
            break;
        }
    } while (x != 0);
    return 0;
}