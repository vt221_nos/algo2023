﻿#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <windows.h>

#define MAX_STACK_SIZE 100

typedef struct {
    int top;
    double data[MAX_STACK_SIZE];
} Stack;

void sent(Stack* stack, double value) {
    if (stack->top >= MAX_STACK_SIZE) {
        printf("Error: Stack overflow\n");
        exit(1);
    }
    stack->data[++stack->top] = value;
}

double pop(Stack* stack) {
    if (stack->top < 0) {
        printf("Помилка: Stack underflow\n");
        exit(1);
    }
    return stack->data[stack->top--];
}

int is_operator(char c) {
    return c == '+' || c == '-' || c == '*' || c == '/' || c == '^' || c == 's';
}

double apply_operator(char znach, double a, double b) {
    switch (znach) {
    case '+': return a + b;
    case '-': return a - b;
    case '*': return a * b;
    case '/': return a / b;
    case '^': return pow(a, b);
    case 's': return sqrt(a);
    }
}

double evaluate_expres(char* expres) {
    Stack stack = { -1 };
    char* p = expres;
    while (*p != '\0') {
        if (*p == ' ') {
            
        }
        else if (isdigit(*p)) {
            sent(&stack, atof(p));
            while (isdigit(*p) || *p == '.') {
                p++;
            }
            p--;
        }
        else if (is_operator(*p)) {
            double b = pop(&stack);
            double a = pop(&stack);
            sent(&stack, apply_operator(*p, a, b));
        }
        else {
            printf("Помилка: Невідомий знак '%c'\n", *p);
            exit(1);
        }
        p++;
    }
    return pop(&stack);
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    Stack stack = { -1, {0} };
    char expres[100];
    printf("Введіть арифметичний приклад для польського запису: ");
    fgets(expres, 100, stdin);
    expres[strcspn(expres, "\n")] = '\0';
    double res = evaluate_expres(expres);
    printf("Результат: %g\n", res);
    return 0;
}
