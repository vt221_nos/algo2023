﻿#include <iostream>
#include <cstdlib>
#include <ctime>
#include <windows.h>
#include <stdio.h>

using namespace std;

void shellSort(float arr[], int n) {
    for (int inc = (n - 1) / 2; inc > 0; inc = (inc - 1) / 2) {
        for (int i = inc; i < n; i += 1) {
            float temp = arr[i];


             
                int j;
            for (j = i; j >= inc && arr[j - inc] > temp; j -= inc) {
                arr[j] = arr[j - inc];
            }

            arr[j] = temp;
        }
    }
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    const int size = 10000;
    float arr[size],result;
    clock_t start, finish;

        // заповнюєм масив рандомними числами из проміжку [0, 400]
        srand(time(NULL));
    for (int i = 0; i < size; i++) {
        arr[i] = (rand() % 400) + (float)rand() / RAND_MAX;
    }

    printf("Масив до сортування:\n");
    for (int i = 0, d = 0; i < size; i++) {
        d++;
        printf("%.4f    ", arr[i]);
        if (d == 10) { printf("\n"); d = 0; }
    }
    start = clock();
    shellSort(arr, size);
    finish = clock();
    printf("Масив після сортування:\n");

    for (int i = 0, d = 0; i < size; i++) {
        d++;
        printf("%.4f    ", arr[i]);
        if (d == 10) { printf("\n"); d = 0; }
    }
    result = (float)(finish - start) / CLOCKS_PER_SEC;
    printf("\nЧас виконання програми: %.4f секунд", result);
    return 0;
}






