﻿#include <iostream>
#include <cstdlib>
#include <ctime>
#include <windows.h>
#include <stdio.h>

using namespace std;

void countingSort(char arr[], int n, int min, int max) {
    int range = max - min + 1;
    int* count = new int[range](); // ініціалізуємо масив count нулями

    // знаходимо кількість входжень кожного елементу масиву arr
    for (int i = 0; i < n; i++) {
        count[arr[i] - min]++;
    }

    // розраховуємо кількість елементів, менших або рівних поточному
    for (int i = 1; i < range; i++) {
        count[i] += count[i - 1];
    }

    // створюємо відсортований масив
    char* sorted = new char[n];
    for (int i = n - 1; i >= 0; i--) {
        sorted[--count[arr[i] - min]] = arr[i];
    }

    // копіюємо відсортований масив назад в оригінальний масив arr
    for (int i = 0; i < n; i++) {
        arr[i] = sorted[i];
    }

    delete[] count;
    delete[] sorted;
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    const int size = 10000;
    char arr[size];
    clock_t start, finish;
    float result;

    // заповнюємо масив випадковими числами з діапазону [-10, 100]
    srand(time(NULL));
    for (int i = 0; i < size; i++) {
        arr[i] = (rand() % 111) - 10;
    }

    printf("Масив до сортування:\n");
    for (int i = 0, d = 0; i < size; i++) {
        d++;
        printf("%d ", arr[i]);
        if (d == 10) { printf("\n"); d = 0; }
    }
    start = clock();
    countingSort(arr, size, -10, 100);
    finish = clock();
    printf("Масив після сортування:\n");

    for (int i = 0, d = 0; i < size; i++) {
        d++;
        printf("%d ", arr[i]);
        if (d == 10) { printf("\n"); d = 0; }
    }
    result = (float)(finish - start) / CLOCKS_PER_SEC;
    printf("\nЧас виконання програми: %.4f секунд", result);
    return 0;
}
