﻿#include <iostream>
#include <cstdlib>
#include <ctime>
#include <windows.h>

using namespace std;

void heapify(double arr[], int n, int i) {
    int largest = i;
    int l = 2 * i + 1;
    int r = 2 * i + 2;

    if (l < n && arr[l] > arr[largest])
        largest = l;

    if (r < n && arr[r] > arr[largest])
        largest = r;

    if (largest != i) {
        swap(arr[i], arr[largest]);

        heapify(arr, n, largest);
    }
}

void heapSort(double arr[], int n) {
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

    for (int i = n - 1; i > 0; i--) {
        swap(arr[0], arr[i]);

        heapify(arr, i, 0);
    }
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    const int size = 10000;
    double arr[size];
    clock_t start, finish;
    float result;

    // заповнюєм масив рандомними числами из проміжку [-100, 10]
    srand(time(NULL));
    for (int i = 0; i < size; i++) {
        arr[i] = (rand() % 111 - 100) + (double)rand() / RAND_MAX;
    }

    printf("Масив до сортування:\n");
    for (int i = 0,d=0; i < size; i++) {
        d++;
        printf("%.4f    ",arr[i]);
        if (d == 10) { printf("\n"); d = 0; }
    }
    start = clock();
    heapSort(arr, size);
    finish = clock();
    printf("Масив після сортування:\n");
  
    for (int i = 0,d=0; i < size; i++) {
        d++;
        printf("%.4f    ", arr[i]);
        if (d == 10) { printf("\n"); d = 0; }
    }
    result= (float)(finish - start) / CLOCKS_PER_SEC;
    printf("\nЧас виконання програми: %.4f секунд", result);

    return 0;
}
