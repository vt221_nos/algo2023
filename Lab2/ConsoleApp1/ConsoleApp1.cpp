﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define N 1000 // розмір масиву псевдовипадкових чисел

// функція генерування псевдовипадкових чисел
int rand_int(int a, int c, int m, int x0)
{
    static int x = x0;
    x = (a * x + c) % m;
    return x;
}

int main()
{
    int a = 1664525, c = 1013904223, m = pow(2, 32), x0 = time(NULL); // параметри конгруентного методу
    int rand_nums[N]; // масив псевдовипадкових чисел
    int freq[N] = { 0 }; // масив частот інтервалів появи випадкових величин
    double prob[N] = { 0 }; // масив статистичної імовірності появи випадкових величин
    double mean = 0.0, var = 0.0, std_dev = 0.0; // змінні для розрахунку мат. сподівання, дисперсії та СКВ

    // генерування послідовності псевдовипадкових чисел та розрахунок частот інтервалів появи
    for (int i = 0; i < N; i++)
    {
        rand_nums[i] = rand_int(a, c, m, x0);
        freq[rand_nums[i] % N]++;
    }

    // розрахунок статистичної імовірності появи та мат. сподівання
    for (int i = 0; i < N; i++)
    {
        prob[i] = (double)freq[i] / N;
        mean += prob[i] * i;
    }

    // розрахунок дисперсії та СКВ
    for (int i = 0; i < N; i++)
    {
        var += prob[i] * pow(i - mean, 2);
    }
    std_dev = sqrt(var);

    // виведення результатів
    printf("Frequency:\n");
    for (int i = 0; i < N; i++)
    {
        printf("%d: %d\n", i, freq[i]);
    }

    printf("\nProbability:\n");
    for (int i = 0; i < N; i++)
    {
        printf("%d: %.5f\n", i, prob[i]);
    }

    printf("\nMean: %.2f\n", mean);
    printf("Variance: %.2f\n", var);
    printf("Standard Deviation: %.2f\n", std_dev);
    return 0;
}